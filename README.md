# Mad comments 
This is a list of comments in code, Git commits or documentation that demonstrates some of the crazy things in this industry. MRs welcome!

* https://github.com/mpv-player/mpv/commit/1e70e82baa91
* https://github.com/gco/xee/blob/master/XeePhotoshopLoader.m#L108
* https://web.archive.org/web/20160402184057/https://github.com/ParsePlatform/parse-server/issues/1050
